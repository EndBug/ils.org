---
layout: post
title: Quattro Anni di Attività
created: 1436635565
---
<p>
Quattro anni fa, a luglio del 2011, abbiamo iniziato a tenere traccia delle attività pro-linuxare svolte in Italia da parte dei LUG e segnalate sui rispettivi siti (aggregati su <a href="http://planet.linux.it/lug/">Planet LUG</a>), pubblicandole sul <a href="http://lugmap.linux.it/eventi/">Calendario Eventi</a> e dandone notifica, provincia per provincia, agli iscritti alla <a href="/newsletter">newsletter ILS</a>.
</p>
<p>
In tale intervallo di tempo più di 1200 eventi sono stati intercettati, in ogni regione e di ogni tipo, di natura a volte tecnica e a volte divulgativa: corsi, incontri occasionali, serate a tema, cene e pizzate, banchetti informativi...
</p>
<p>
Per celebrare i quattro anni di questa iniziativa è stato realizzato un piccolo video che riassume in due minuti come si sono distribuite nello spazio e nel tempo tali attività, utili ed anzi indispensabili per far conoscere il software libero ad un numero sempre crescente di persone e per coinvolgere nuovi volontari - sia nella propaganda che nello sviluppo.
</p>
<p>
Grazie a tutti i gruppi e a tutte le associazioni che ogni giorno operano nelle rispettive città per promuovere i valori della consapevolezza digitale!
</p>

<div style="margin: auto">
<iframe width="590" height="472" src="https://www.youtube-nocookie.com/embed/KsF394dPIIg?rel=0" frameborder="0" allowfullscreen></iframe>
</div>
