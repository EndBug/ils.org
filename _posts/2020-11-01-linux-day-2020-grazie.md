---
layout: post
title: 'Linux Day 2020: Grazie!'
created: 1604230006
image: /assets/posts/images/disco.jpg
image_copy: Photo by <a rel="nofollow" href="https://unsplash.com/@greysonjoralemon?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Greyson Joralemon</a> on <a rel="nofollow" href="https://unsplash.com/s/photos/party?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Unsplash</a>
---

Sabato 24 e domenica 25 ottobre si è svolto il Linux Day 2020, in una <a href="{% link _posts/2020-11-01-linux-day-2020-grazie.md %}">inedita veste online</a>.

È difficile stimare il numero di partecipanti reali e complessivi, ma si possono dare alcuni numeri per farsi una idea dell'impatto della manifestazione:

<!--more-->

<ul>
    <li>8000 visite al sito <a href="https://www.linuxday.it/2020/">www.linuxday.it/2020</a> nei due giorni</li>
    <li>1400 utenti sulla chat</li>
    <li>una media globale di 340 viewer contemporanei sulle 16 ore di diretta streaming</li>
</ul>

Questo aggiornamento è però dedicato soprattutto a ringraziare le persone che hanno reso possibile questa iniziativa, non senza qualche sforzo e qualche sacrificio.

Staff tecnico:
<ul>
    <li>Rosario Antoci (istanza Rocket Chat e istanza PeerTube)</li>
    <li>Fabio Moriondo (istanze Big Blue Button)</li>
    <li>Michele Tameni (streaming)</li>
    <li>tante persone che lavorano presso l'associazione GARR, nei team di <a rel="nofollow" href="https://cloud.garr.it/">GARR Cloud</a> e di <a rel="nofollow" href="https://garr.tv/">GARR.tv</a>, ed in particolare Massimo Carboni</li>
</ul>

Presentatori delle sessioni:
<ul>
    <li>Francesco Alaimo (sessione Scuola, pomeriggio)</li>
    <li>Stefania Delprete (sessione Applicazioni, mattino)</li>
    <li>Giovanni Dodero (sessione Applicazioni, pomeriggio)</li>
    <li>Edoardo Maria Elidoro (sessione Programmazione e sessione SysOps)</li>
    <li>Roberto Guido (sessione Base)</li>
    <li>Paolo Mauri (sessione Scuola, mattino)</li>
    <li>Daniele Scasciafratte (sessione Approfondimenti)</li>
</ul>

Moderatori in chat:
<ul>
    <li>Francesco Ariis @f-a</li>
    <li>Andrea Laisa @amreo</li>
    <li>Floriana Tabone @midna</li>
</ul>

Un ringraziamento va anche ai <a href="/info#sponsor">sostenitori di Italian Linux Society</a>, che ci danno una mano per le spese (in particolare, nel contesto del Linux Day, per i costi di produzione e spedizione delle quasi 200 richieste di <a href="https://www.linux.it/stickers">stickers</a> arrivate in due giorni):
<ul>
    <li><a rel="nofollow" href="http://www.applebyitalia.it/">Appleby</a></li>
    <li><a rel="nofollow" href="https://continuity.space/">Continuity</a></li>
    <li><a rel="nofollow" href="https://www.ergonet.it/">Ergonet</a></li>
    <li><a rel="nofollow" href="https://www.lpi.org/it/">Linux Professional Institute</a></li>
    <li><a rel="nofollow" href="https://softwareworkers.it/">Software Workers</a></li>
</ul>

Grazie infine a tutti coloro che hanno supportato e promosso questa inedita iniziativa, dal suo concepimento all'implementazione.
