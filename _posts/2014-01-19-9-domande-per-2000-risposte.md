---
layout: post
title: 9 Domande per 2000 Risposte
created: 1390099416
---
Siamo lieti di annunciare che la newsletter di <a href="/">Italian Linux Society</a>, inaugurata contestualmente alla massiccia revisione del sito operata <a href="{% link _posts/2013-03-26-di-nuovo-online.md %}">un anno fa</a>, ha recentemente toccato quota 2000 iscritti: un bel numero di persone - in costante crescita - interessate al mondo del software libero e nella maggior parte dei casi alle iniziative locali notificate per mezzo del <a href="http://www.linux.it/eventi">LugCalendar</a>.

Approfittiamo di questo ampio - e per certi versi inedito - pubblico per proporre un questionario, cui appunto gli iscritti alla newsletter sono i primi invitati ma comunque aperto a tutti, con due diversi obiettivi: raccogliere qualche informazione saporitamente statistica su chi nel nostro Paese ha il pallino per Linux, e collezionare consigli utili per perfezionare le nostre attivita' presenti e future. Poche e semplici domande di carattere genericamente anagrafico, sul proprio coinvolgimento all'interno della community, e sulla propria opinione su alcuni temi - grandi e piccoli - di interesse primario; pochi minuti per dare un contributo e fornire dati e spunti da cui far emergere idee e progetti con cui continuare a promuovere il software libero in Italia.

Il questionario <a rel="nofollow" href="http://www.linuxday.it/questionario/index.php/734973?lang-it">e' reperibile qui</a>, in caso di problemi di accesso o compilazione e' possibile contattare l'indirizzo mail <a href="mailto:direttore@linux.it">direttore@linux.it</a> per assistenza.

Come sempre i risultati saranno pubblicati, in forma aggregata, nei prossimi mesi.
