---
layout: post
title: Congresso AIP 2013
created: 1369595047
---
<p>
  Italian Linux Society è lieta di patrocinare il <a rel="nofollow" href="http://verona2013.aipnet.it/">Congresso 2013 dell'Associazione Informatici Professionisti</a>, che si terrà a Verona venerdi 7 e giovedi 8 giugno. In tale occasione uno dei nostri più recenti consiglieri, Paolo Foletto, terrà uno stand informativo sull'associazione e sul ruolo che il software libero può avere nel mondo dell'impresa, soprattutto in un periodo storico in cui è importante riuscire a trovare il giusto equilibrio tra innovazione e costi.
</p>
<p>
  L'esperienza è pressoché inedita per Italian Linux Society, che in questa sede muove la propria presenza al di fuori del ristretto cerchio delle manifestazioni più strettamente rivolte alla promozione a Linux per andare ad incontrare direttamente gli operatori del settore informatico. Facendolo peraltro in grande stile, collaborando con una delle più solide e consolidate entità nel suo genere, appunto l'<a rel="nofollow" href="http://www.aipnet.it/">Associazione Informatici Professionisti</a>. Una importante opportunità di confronto con la realtà professionale ed imprenditoriale del nostro Paese, e di dialogo sia con coloro che già impiegano soluzioni libere per il proprio business - riconoscendone il valore aggiunto in termini di interoperabilità ed efficienza - sia con coloro che ancora non hanno fatto il grande passo (e magari comprenderne le motivazioni).
</p>
<p>
  L'iniziativa si pone come primo passo di una campagna di divulgazione ed estensione dell'attività ILS, rivolta a portare le tematiche a noi care presso il pubblico più ampio possibile.
</p>
<p>
  Un ringraziamento ad AIP per l'invito e la disponibilità, con l'augurio che altre analoghe collaborazioni possano sbocciare e fiorire nel prossimo futuro.
</p>
