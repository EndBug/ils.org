# Sito ILS

Questo è il repository del sito www.ils.org, generato con [Jekyll](https://jekyllrb.com/).

Ogni volta che viene effettuata una modifica occorre rieseguire `JEKYLL_ENV=production bundle exec jekyll build` per ricompilare l'intero sito. Attenzione: questo incorpora nelle pagine dei post il widget per i commenti Discourse.

In fase di sviluppo è possibile eseguire `bundle exec jekyll serve --livereload`.

Per segnalazioni e richieste, scrivi a webmaster@linux.it
